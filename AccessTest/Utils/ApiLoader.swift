//
//  ApiLoader.swift
//  AccessTest
//
//  Created by Sean on 2021/6/12.
//

import Foundation
import Alamofire

class ApiLoader {
    let userListApi = "https://api.github.com/users"
    
    func loadUserListData<T: Codable>(since: Int, perPage: Int, completion:(@escaping (Result<[T], Error>) -> Void)) {
        let param = ["since": String(since), "per_page": String(perPage)]
        
        _ = AF.request(userListApi, parameters: param).responseDecodable(of: Array<T>.self) { response in
            switch response.result {
            case .success(let datas):
                completion(.success(datas))
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
    }
    
    func loadUserInfo<T: Codable>(userName: String, completion:(@escaping (Result<T, Error>) -> Void)) {
        let userInfoApi = "\(userListApi)/\(userName)"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        _ = AF.request(userInfoApi).responseDecodable(of: T.self, decoder: decoder) { response in
            switch response.result {
            case .success(let datas):
                completion(.success(datas))
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
    }
    
}

//
//  UserProfileTableViewCell.swift
//  AccessTest
//
//  Created by Sean on 2021/6/14.
//

import UIKit

class UserProfileTableViewCell: UITableViewCell, CellConfigurable {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var badge: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        layout()
    }
    
    func layout() {
        badge.layer.cornerRadius = badge.frame.height / 2
    }

    func setup(viewModel: CellViewModelProtocol) {
        if let viewModel = viewModel as? UserProfileCellViewModel {
            self.name.text = viewModel.name
            self.badge.isHidden = !viewModel.badge
        }
    }
    
}

//
//  UserImageTableViewCell.swift
//  AccessTest
//
//  Created by Sean on 2021/6/14.
//

import UIKit
import AlamofireImage

class UserImageTableViewCell: UITableViewCell, CellConfigurable {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var bio: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layout()
    }
    
    func layout() {
        userImage.layer.cornerRadius = userImage.frame.height / 2
    }
    
    func setup(viewModel: CellViewModelProtocol) {
        if let viewModel = viewModel as? UserImageCellViewModel {
            self.name.text = viewModel.name
            self.bio.text = viewModel.bio
            if let url = URL(string: viewModel.imageUrl) {
                userImage.af.setImage(withURL: url)
            }
        }
    }
}

//
//  UserInfoViewController.swift
//  AccessTest
//
//  Created by Sean on 2021/6/13.
//

import UIKit

class UserInfoViewController: UIViewController, ControllerConfigurable {
    
    @IBOutlet weak var userInfoTableView: UITableView!
    var viewModel = UserInfoViewModel(userName: "")
    

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func setup(viewModel: ViewModelProtocol) {
        if let viewModel = viewModel as? UserInfoViewModel{
            self.viewModel = viewModel
        }
    }

}

extension UserInfoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsInSection(section: section);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = self.viewModel.cellViewModelForRowAt(indexPath: indexPath)
        var cell = tableView.dequeueReusableCell(withIdentifier: cellViewModel.cellIdentifier())
        if cell == nil {
            tableView.register(UINib(nibName: cellViewModel.cellIdentifier(), bundle: Bundle.main), forCellReuseIdentifier: cellViewModel.cellIdentifier())
            cell = tableView.dequeueReusableCell(withIdentifier: cellViewModel.cellIdentifier())
        }
        if let cell = cell as? CellConfigurable {
            cell.setup(viewModel: cellViewModel)
        }
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }
    
}

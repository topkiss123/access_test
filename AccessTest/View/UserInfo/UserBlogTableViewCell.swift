//
//  UserBlogTableViewCell.swift
//  AccessTest
//
//  Created by Sean on 2021/6/14.
//

import UIKit

class UserBlogTableViewCell: UITableViewCell, CellConfigurable {

    @IBOutlet weak var blogTextView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func setup(viewModel: CellViewModelProtocol) {
        if let viewModel = viewModel as? UserBlogCellViewModel {
            self.blogTextView.text = viewModel.blog
        }
    }
    
}

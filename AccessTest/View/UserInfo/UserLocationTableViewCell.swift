//
//  UserLocationTableViewCell.swift
//  AccessTest
//
//  Created by Sean on 2021/6/14.
//

import UIKit

class UserLocationTableViewCell: UITableViewCell, CellConfigurable {

    @IBOutlet weak var location: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(viewModel: CellViewModelProtocol) {
        if let viewModel = viewModel as? UserLocationCellViewModel {
            self.location.text = viewModel.location
        }
    }
    
}

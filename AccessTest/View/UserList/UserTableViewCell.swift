//
//  UserTableViewCell.swift
//  AccessTest
//
//  Created by Sean on 2021/6/13.
//

import UIKit
import AlamofireImage

class UserTableViewCell: UITableViewCell, CellConfigurable {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var badge: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layout()
    }
    
    func layout() {
        userImage.layer.cornerRadius = userImage.frame.height / 2
        badge.layer.cornerRadius = badge.frame.height / 2
        bgView.layer.borderWidth = 1
        bgView.layer.borderColor = UIColor.lightGray.cgColor
        bgView.layer.shadowOffset = CGSize(width: 0, height: 2)
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowOpacity = 0.8
    }

    func setup(viewModel: CellViewModelProtocol) {
        if let viewModel = viewModel as? UserCellViewModel {
            self.name.text = viewModel.name
            self.badge.isHidden = !viewModel.badge
            if let url = URL(string: viewModel.imageUrl) {
                userImage.af.setImage(withURL: url)
            }
        }
    }
    
}

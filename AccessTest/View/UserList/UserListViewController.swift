//
//  UserListViewController.swift
//  AccessTest
//
//  Created by Sean on 2021/6/12.
//

import UIKit

class UserListViewController: UIViewController {

    @IBOutlet weak var userListTableView: UITableView!
    let viewModel = UserListViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getUserList()
        viewModel.tableViewReload = {[weak self] in
            self?.userListTableView.reloadData()
        }
    }

}

extension UserListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsInSection(section: section);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = self.viewModel.cellViewModelForRowAt(indexPath: indexPath)
        var cell = tableView.dequeueReusableCell(withIdentifier: cellViewModel.cellIdentifier())
        if cell == nil {
            tableView.register(UINib(nibName: cellViewModel.cellIdentifier(), bundle: Bundle.main), forCellReuseIdentifier: cellViewModel.cellIdentifier())
            cell = tableView.dequeueReusableCell(withIdentifier: cellViewModel.cellIdentifier())
        }
        if let cell = cell as? CellConfigurable {
            cell.setup(viewModel: cellViewModel)
        }
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewModel.loadNextPage(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userInfoViewModel = UserInfoViewModel(userName: self.viewModel.getUserName(indexPath: indexPath))
        userInfoViewModel.loadUserInfo { [weak self] in
            let userInfoVC = UserInfoViewController()
            userInfoVC.setup(viewModel: userInfoViewModel)
            self?.navigationController?.pushViewController(userInfoVC, animated: true)
        }
    }
    
}


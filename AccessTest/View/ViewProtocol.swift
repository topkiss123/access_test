//
//  ViewProtocol.swift
//  AccessTest
//
//  Created by Sean on 2021/6/12.
//

import Foundation

protocol ControllerConfigurable{
    func setup(viewModel: ViewModelProtocol)
}

protocol CellConfigurable{
    func setup(viewModel: CellViewModelProtocol)
}

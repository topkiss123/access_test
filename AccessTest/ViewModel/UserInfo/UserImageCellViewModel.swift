//
//  UserImageCellViewModel.swift
//  AccessTest
//
//  Created by Sean on 2021/6/14.
//

import Foundation

class UserImageCellViewModel: CellViewModelProtocol {
    
    let name: String
    let bio: String
    let imageUrl: String
    
    init(name: String, bio: String, imageUrl: String) {
        self.name = name
        self.bio = bio
        self.imageUrl = imageUrl
    }
    
    func cellIdentifier() -> String {
        return String(describing: UserImageTableViewCell.self)
    }
}

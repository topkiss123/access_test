//
//  UserLocationCellViewModel.swift
//  AccessTest
//
//  Created by Sean on 2021/6/14.
//

import Foundation

class UserLocationCellViewModel: CellViewModelProtocol {
    
    let location: String
    
    init(location: String) {
        self.location = location
    }
    
    func cellIdentifier() -> String {
        return String(describing: UserLocationTableViewCell.self)
    }
}

//
//  UserInfoViewModel.swift
//  AccessTest
//
//  Created by Sean on 2021/6/13.
//

import Foundation

class UserInfoViewModel: ViewModelProtocol {
    
    let userName: String
    private var userInfo: User? {
        didSet {
            prapareCellViewModel()
        }
    }
    private let apiLoader = ApiLoader()
    private var cellViewModels = [CellViewModelProtocol]()
    
    init(userName: String) {
        self.userName = userName
        self.userInfo = nil
    }
    
    func loadUserInfo(completion:(@escaping () -> Void)) {
        apiLoader.loadUserInfo(userName: self.userName) { (result: Result<User, Error>) in
            switch result {
            case .success(let datas):
                DispatchQueue.main.async { [weak self]  in
                    self?.userInfo = datas
                    completion()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func prapareCellViewModel() {
        if let userInfo = self.userInfo {
            let userImageCellViewModel = UserImageCellViewModel(name: userInfo.name ?? "", bio: userInfo.bio ?? "", imageUrl: userInfo.avatar_url)
            self.cellViewModels.append(userImageCellViewModel)
            
            let userProfileCellViewModel = UserProfileCellViewModel(name: userInfo.login, badge: userInfo.site_admin)
            self.cellViewModels.append(userProfileCellViewModel)
            
            let userLocationCellViewModel = UserLocationCellViewModel(location: userInfo.location ?? "")
            self.cellViewModels.append(userLocationCellViewModel)
            
            let userBlogCellViewModel = UserBlogCellViewModel(blog: userInfo.blog ?? "")
            self.cellViewModels.append(userBlogCellViewModel)
        }
    }
    
    
    func numberOfRowsInSection(section: NSInteger) -> NSInteger {
        return self.cellViewModels.count
    }
    
    func numberOfSections() -> NSInteger {
        return 1
    }
    
    func cellViewModelForRowAt(indexPath: IndexPath) -> CellViewModelProtocol {
        return self.cellViewModels[indexPath.row]
    }
}

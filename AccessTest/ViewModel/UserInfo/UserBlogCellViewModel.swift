//
//  UserBlogCellViewModel.swift
//  AccessTest
//
//  Created by Sean on 2021/6/14.
//

import Foundation

class UserBlogCellViewModel: CellViewModelProtocol {
    
    let blog: String
    
    init(blog: String) {
        self.blog = blog
    }
    
    func cellIdentifier() -> String {
        return String(describing: UserBlogTableViewCell.self)
    }
}

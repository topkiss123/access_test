//
//  UserProfileCellViewModel.swift
//  AccessTest
//
//  Created by Sean on 2021/6/14.
//

import Foundation

class UserProfileCellViewModel: CellViewModelProtocol {
    
    let name: String
    let badge: Bool
    
    init(name: String, badge: Bool) {
        self.name = name
        self.badge = badge
    }
    
    func cellIdentifier() -> String {
        return String(describing: UserProfileTableViewCell.self)
    }
}

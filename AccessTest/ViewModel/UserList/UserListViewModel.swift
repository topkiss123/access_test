//
//  UserListViewModel.swift
//  AccessTest
//
//  Created by Sean on 2021/6/13.
//

import Foundation

class UserListViewModel: ViewModelProtocol {

    private var users = [User]()
    private var currentIndax = 0
    private var perPage = 20
    private let apiLoader = ApiLoader()
    
    var tableViewReload: (() -> Void)?
    
    
    func getUserList() {
        apiLoader.loadUserListData(since: currentIndax, perPage: perPage) { (result: Result<[User], Error>) in
            switch result {
            case .success(let datas):
                DispatchQueue.main.async { [weak self]  in
                    self?.users.append(contentsOf: datas)
                    if let reload = self?.tableViewReload {
                        reload()
                    }
                }
            case .failure(let error):
                print(error)
            }
        };
    }
    
    func getUserName(indexPath: IndexPath) -> String {
        return self.users[indexPath.row].login
    }
    
    func loadNextPage(indexPath: IndexPath) {
        if indexPath.row == (users.count - 1) {
            currentIndax = users.last!.id
            getUserList()
        }
    }
    
    func numberOfRowsInSection(section: NSInteger) -> NSInteger {
        return self.users.count
    }
    
    func cellViewModelForRowAt(indexPath: IndexPath) -> CellViewModelProtocol {
        let user = self.users[indexPath.row]
        let cellViewModel = UserCellViewModel(name:user.login, imageUrl: user.avatar_url, badge: user.site_admin)
        return cellViewModel
    }
    
    func numberOfSections() -> NSInteger {
        return 1
    }
    
}

//
//  UserCellViewModel.swift
//  AccessTest
//
//  Created by Sean on 2021/6/13.
//

import Foundation
import UIKit

class UserCellViewModel: CellViewModelProtocol {
    
    let name: String
    let badge: Bool
    let imageUrl: String
    
    init(name: String, imageUrl: String, badge: Bool) {
        self.name = name
        self.imageUrl = imageUrl
        self.badge = badge
    }
    
    func cellIdentifier() -> String {
        return String(describing: UserTableViewCell.self)
    }
}

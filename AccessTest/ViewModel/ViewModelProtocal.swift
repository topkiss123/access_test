//
//  ViewModelProtocal.swift
//  AccessTest
//
//  Created by Sean on 2021/6/13.
//

import Foundation

protocol ViewModelProtocol {
    func numberOfRowsInSection(section: NSInteger) -> NSInteger
    func numberOfSections() -> NSInteger
    func cellViewModelForRowAt(indexPath: IndexPath) -> CellViewModelProtocol
}

protocol CellViewModelProtocol {
    func cellIdentifier() -> String
}

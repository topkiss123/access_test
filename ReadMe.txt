1. Third party libs:
Cocoapod: Use Alamofire and AlamofireImage.

2. Project
Utils/ApiLoader.swift: 
	Provide two methods, ApiLoader.loadUserListData() is used to get user list, that needs two parameters, 'since' and 'perPage', and callback when receives the response. the other named ApiLoader.loadUserInfo() is used to get user's detail, that needs one parameter, 'userName', and callback when receives the response.
Model/UserList/User.swift: 
	Implement codable protocol and stored the data.
ViewModel/ViewModelProtocal.swift: 
	It's a protocol that view model instance to implement
ViewModel/UserList/UserListViewModel.swift: 
	It's a view model for UserListViewController.swift. It uses the ApiLoader.swift to get data. Provide methods for view controller.
ViewModel/UserList/UserInfoViewModel.swift: 
	It's a view model for UserInfoViewController.swift. It uses the ApiLoader.swift to get data. Provide methods for view controller.
View/ViewProtocal.swift: 
	It's a protocol that view instance to implement.
View/UserList/UserListViewController.swift: 
	It's the first page. Uses table view to perform layout. Combine the cell and the cell view model.
View/UserList/UserInfoViewController.swift: 
	It's the second page. Uses table view to perform layout. Combine the cell and the cell view model.

Cell and cell view model mapping:
	UserTableViewCell -> UserCellViewModel
	UserImageTableViewCell -> UserImageCellViewModel
	UserProfileTableViewCell -> UserProfileCellViewModel
	UserLocationTableViewCell -> UserLocationCellViewModel
	UserBlogTableViewCell -> UserBlogCellViewModel

